package by.itstep.job_search.dto.userDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserShortDto {

    private Integer id;
    private String firstName;
    private String lastName;
    private Integer yearsOfExperience;
    private String position;

}
