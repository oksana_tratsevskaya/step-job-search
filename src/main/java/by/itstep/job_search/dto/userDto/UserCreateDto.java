package by.itstep.job_search.dto.userDto;

import by.itstep.job_search.enam.RoleOfUser;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
public class UserCreateDto {

    @NotNull(message = "FirstName can't be null")
    private String firstName;

    @NotNull(message = "LastName can't be null")
    private String lastName;

    @NotNull(message = "Years of experience can't be null")
    private Integer yearsOfExperience;

    @NotNull(message = "Position can't be null")
    private String position;

    @NotNull(message = "Phone can't be null")
    private String phone;

    @NotNull(message = "Email can't be null")
    @Email(message = "Email must be valid")
    private String email;

    @NotNull(message = "Role can't be null")
    private RoleOfUser role;

    @NotNull(message = "Password can't be null")
    @Size(min = 6, max = 100, message = "Password length must be between 6 and 100")
    private String password;

}
