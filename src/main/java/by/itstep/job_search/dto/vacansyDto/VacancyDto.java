package by.itstep.job_search.dto.vacansyDto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@Builder
public class VacancyDto {

    @NotNull(message = "Id can't be null")
    Integer id;

    @NotNull(message = "Name can't be null")
    String name;

    @NotNull(message = "Position can't be null")
    String position;

    @NotNull(message = "Salary can't be null")
    Integer salary;

    @NotNull(message = "CompanyName can't be null")
    String companyName;

}
