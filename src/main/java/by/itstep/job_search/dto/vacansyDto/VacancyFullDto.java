package by.itstep.job_search.dto.vacansyDto;

import by.itstep.job_search.dto.userDto.UserDto;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VacancyFullDto {

    Integer id;
    String name;
    String position;
    Integer salary;
    String companyName;
    UserDto userId;

}
