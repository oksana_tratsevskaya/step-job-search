package by.itstep.job_search.dto.vacansyDto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VacancyCreateDto {

    String name;
    String position;
    Integer salary;
    String companyName;
    Integer userId;

}
