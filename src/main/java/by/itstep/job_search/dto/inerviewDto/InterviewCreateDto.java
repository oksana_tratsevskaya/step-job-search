package by.itstep.job_search.dto.inerviewDto;

import by.itstep.job_search.dto.userDto.UserDto;
import by.itstep.job_search.dto.userDto.UserUpdateDto;
import by.itstep.job_search.dto.vacansyDto.VacancyDto;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
@Builder
public class InterviewCreateDto {

    @NotNull(message = "Date can't be null")
    Date date;

    @NotNull(message = "Vacancy's ID can't be null")
    Integer vacancyId;

    @NotNull(message = "User' ID can't be null")
    Integer userId;

}
