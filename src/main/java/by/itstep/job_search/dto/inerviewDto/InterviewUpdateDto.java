package by.itstep.job_search.dto.inerviewDto;

import by.itstep.job_search.enam.StatusOfInterview;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Data
@Builder
public class InterviewUpdateDto {

    @DecimalMin (value = "1", message = "ID can't be null")
    Integer id;

    @NotNull(message = "Status can't be null")
    StatusOfInterview status;

    @NotNull(message = "Date can't be null")
    @FutureOrPresent(message = "Date cannot be past")
    Date date;

}
