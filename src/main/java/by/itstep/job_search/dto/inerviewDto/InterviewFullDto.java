package by.itstep.job_search.dto.inerviewDto;

import by.itstep.job_search.dto.userDto.UserDto;
import by.itstep.job_search.dto.vacansyDto.VacancyDto;
import by.itstep.job_search.enam.StatusOfInterview;
import lombok.Builder;
import lombok.Data;

import java.sql.Date;

@Data
@Builder
public class InterviewFullDto {

    Integer id;
    StatusOfInterview status;
    Date date;
    VacancyDto vacancy;
    UserDto user;

}
