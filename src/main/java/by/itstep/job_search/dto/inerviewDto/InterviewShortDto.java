package by.itstep.job_search.dto.inerviewDto;

import by.itstep.job_search.enam.StatusOfInterview;
import lombok.Builder;
import lombok.Data;

import java.sql.Date;

@Data
@Builder
public class InterviewShortDto {

    Integer id;
    StatusOfInterview status;
    Date date;

}
