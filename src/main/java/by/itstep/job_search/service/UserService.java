package by.itstep.job_search.service;

import by.itstep.job_search.dto.userDto.UserCreateDto;
import by.itstep.job_search.dto.userDto.UserDto;
import by.itstep.job_search.dto.userDto.UserShortDto;
import by.itstep.job_search.dto.userDto.UserUpdateDto;
import by.itstep.job_search.entity.InterviewEntity;
import by.itstep.job_search.entity.UserEntity;
import by.itstep.job_search.entity.VacancyEntity;
import by.itstep.job_search.mapper.UserMapper;
import by.itstep.job_search.mapper.mapperStruct.UserMapperStruct;
import by.itstep.job_search.repository.interfaceOfRepository.InterviewRepository;
import by.itstep.job_search.repository.interfaceOfRepository.UserRepository;
import by.itstep.job_search.repository.interfaceOfRepository.VacancyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private InterviewRepository interviewRepository;

    @Autowired
    private VacancyRepository vacancyRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserMapperStruct userMapperStruct;

    public UserDto findById(int id) {
        UserEntity user = userRepository.findById(id);

        if(user == null) {
            throw new RuntimeException("UserService.findById -> User not found by id " + id);
        }

        System.out.println("UserService.findById -> Found user: " + id);

        return userMapperStruct.userEntityToUserDto(user);
    }

    public List<UserShortDto> findAll() {
        List<UserEntity> allUser = userRepository.findAll();

        System.out.println("UserService.findAll -> Found users");
        return  userMapperStruct.userEntitiesToUsersShortDto(allUser);
    }

    public UserShortDto update(UserUpdateDto userUpdateDto) {

        if(userUpdateDto.getId() == null) {
            throw new RuntimeException("UserService.update -> Can't update entity without id ");
        }

        UserEntity entityFromBD = userRepository.findById(userUpdateDto.getId());
        entityFromBD.setFirstName(userUpdateDto.getFirstName());
        entityFromBD.setLastName(userUpdateDto.getLastName());
        entityFromBD.setYearsOfExperience(userUpdateDto.getYearsOfExperience());
        entityFromBD.setPosition(userUpdateDto.getPosition());
        entityFromBD.setPhone(userUpdateDto.getPhone());
        entityFromBD.setRole(userUpdateDto.getRole());

        if (entityFromBD.getRole() == null) {
            throw new RuntimeException("UserService.update -> Role is empty");
        }

        UserEntity updatedUser = userRepository.update(entityFromBD);

        System.out.println("UserService.update -> Updated user: " + updatedUser);

        return userMapperStruct.userEntitiesToUserShortDto(updatedUser);
    }

    public UserDto create(UserCreateDto userCreateDto) {
        List<UserEntity> existingUsers = userRepository.findAll();

        for (UserEntity user : existingUsers) {
            if (user.getEmail().equals(userCreateDto.getEmail())) {
                throw new RuntimeException("UserService.create -> Email " + user.getEmail() + "is taken");
            }
        }

        if (userCreateDto.getRole() == null) {
            throw new RuntimeException("UserService.create -> Role is empty");
        }

        if (userCreateDto.getEmail() == null) {
            throw new RuntimeException("UserService.create -> Email is empty");
        }

        if (userCreateDto.getPassword() == null) {
            throw new RuntimeException("UserService.create -> Password is empty");
        }

        UserEntity createdUser = userRepository.create(userMapperStruct.userDtoToUserEntity(userCreateDto));
        System.out.println("UserService.create -> Created user: " + createdUser);
        return userMapperStruct.userEntityToUserDto(createdUser);
    }

    public String deleteById(int id) {
        UserEntity userToDelete = userRepository.findById(id);
        if (userToDelete == null){
            throw new RuntimeException("UserService.deleteById -> User was not found by id " + id);
        }

        List<InterviewEntity> interviewOfUser = userToDelete.getInterviews();
        List<VacancyEntity> vacancyServices = userToDelete.getVacancies();
        for(InterviewEntity interviewEntity : interviewOfUser) {
            interviewRepository.deleteById(interviewEntity.getId());
            System.out.println("UserService.deleteById -> Interview was deleted");
        }

        for(VacancyEntity vacancy : vacancyServices) {
            vacancyRepository.deleteById(vacancy.getId());
            System.out.println("UserService.deleteById -> Vacancy was deleted");
        }

        userRepository.deleteById(id);
        System.out.println("UserService -> Deleted user: " + id);

        return "User with id#" + id + " was deleted";
    }
}
