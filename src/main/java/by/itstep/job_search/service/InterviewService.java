package by.itstep.job_search.service;

import by.itstep.job_search.dto.inerviewDto.InterviewCreateDto;
import by.itstep.job_search.dto.inerviewDto.InterviewFullDto;
import by.itstep.job_search.dto.inerviewDto.InterviewShortDto;
import by.itstep.job_search.dto.inerviewDto.InterviewUpdateDto;
import by.itstep.job_search.enam.StatusOfInterview;
import by.itstep.job_search.entity.InterviewEntity;
import by.itstep.job_search.mapper.mapperStruct.InterviewMapperStruct;
import by.itstep.job_search.repository.interfaceOfRepository.InterviewRepository;
import by.itstep.job_search.repository.interfaceOfRepository.UserRepository;
import by.itstep.job_search.repository.interfaceOfRepository.VacancyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InterviewService {

    @Autowired
    private InterviewRepository interviewRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VacancyRepository vacancyRepository;

    @Autowired
    protected InterviewMapperStruct interviewMapperStruct;

    public InterviewFullDto findById(int id) {
        InterviewEntity interview = interviewRepository.findById(id);
        if(interview == null) {
            throw new RuntimeException("InterviewService -> Interview not found by id " + id);
        }

        System.out.println("InterviewService -> Found interview: " + id);

        return interviewMapperStruct.interviewEntityToInterviewFullDto(interview);
    }

    public List<InterviewShortDto> findAll() {

        List<InterviewShortDto> allInterview = interviewMapperStruct.interviewEntitiesToInterviewShortDto(interviewRepository.findAll());

        System.out.println("InterviewService -> Found interviews");

        return  allInterview;
    }

    public InterviewFullDto update(InterviewUpdateDto updateDto) {

        if (interviewRepository.findById(updateDto.getId()) == null) {
            throw new RuntimeException("InterviewService -> Interview not found by id " + updateDto.getId());
        }

        InterviewEntity entity = interviewRepository.findById(updateDto.getId());
        entity.setStatus(updateDto.getStatus());
        entity.setDate(updateDto.getDate());

        if(entity.getId() == null) {
            throw new RuntimeException("InterviewService -> Can't update entity without id ");
        }

        if (interviewRepository.findById(entity.getId()) == null) {
            throw new RuntimeException("InterviewService -> Interview not found by id " + entity.getId());
        }

        InterviewEntity updatedInterview = interviewRepository.update(entity);

        System.out.println("InterviewService -> Updated interview: " + entity);

        return interviewMapperStruct.interviewEntityToInterviewFullDto(updatedInterview);
    }

    public InterviewFullDto create(InterviewCreateDto createDto) {

        InterviewEntity entity = interviewMapperStruct.interviewCreateDtoToInterviewEntity(createDto);
        entity.setVacancy(vacancyRepository.findById(createDto.getVacancyId()));
        entity.setUser(userRepository.findById(createDto.getUserId()));

        if (entity.getId() != null) {
            throw new RuntimeException("InterviewService -> Can't create entity with id");
        }

        entity.setStatus(StatusOfInterview.REQUESTED);

        InterviewEntity createdInterview = interviewRepository.create(entity);
        System.out.println("InterviewService -> Created interview: " + entity);

        return interviewMapperStruct.interviewEntityToInterviewFullDto(createdInterview);
    }

    public void deleteById(int id) {
        InterviewEntity interviewToDelete = interviewRepository.findById(id);
        if (interviewToDelete == null){
            throw new RuntimeException("InterviewService -> Interview was not found by id " + id);
        }

        interviewRepository.deleteById(id);
        System.out.println("InterviewService -> Deleted interview: " + id);

    }
}
