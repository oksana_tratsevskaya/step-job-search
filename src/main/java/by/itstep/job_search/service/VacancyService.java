package by.itstep.job_search.service;

import by.itstep.job_search.dto.vacansyDto.VacancyCreateDto;
import by.itstep.job_search.dto.vacansyDto.VacancyDto;
import by.itstep.job_search.dto.vacansyDto.VacancyFullDto;
import by.itstep.job_search.enam.RoleOfUser;
import by.itstep.job_search.entity.InterviewEntity;
import by.itstep.job_search.entity.VacancyEntity;
import by.itstep.job_search.mapper.VacancyMapper;
import by.itstep.job_search.mapper.mapperStruct.VacancyMapperStruct;
import by.itstep.job_search.repository.interfaceOfRepository.InterviewRepository;
import by.itstep.job_search.repository.interfaceOfRepository.UserRepository;
import by.itstep.job_search.repository.interfaceOfRepository.VacancyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VacancyService {

    @Autowired
    private VacancyRepository vacancyRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private InterviewRepository interviewRepository;

    @Autowired
    private VacancyMapper vacancyMapper;

    @Autowired
    private VacancyMapperStruct vacancyMapperStruct;

    public VacancyFullDto findById(int id) {
       VacancyEntity requiredVacancy = vacancyRepository.findById(id);
       if (requiredVacancy == null) {
            throw new RuntimeException("VacancyService -> Vacancy not found by id " + id) ;
       }

        return vacancyMapperStruct.vacancyEntityToVacancyFullDto(requiredVacancy);
    }

    public List<VacancyDto> findAll() {
        List<VacancyEntity> vacancies = vacancyRepository.findAll();

        if(vacancies == null) {
            System.out.println("VacancyService -> Not vacancies");
        }

        return vacancyMapperStruct.vacanciesEntityToVacanciesDto(vacancies);
    }

    public VacancyFullDto update(VacancyDto vacancyDto) {

        if(vacancyDto.getId() == null) {
            throw new RuntimeException("VacancyService -> Can't update entity without id");
        }

        VacancyEntity vacancyEntityFromBD = vacancyRepository.findById(vacancyDto.getId());
        vacancyEntityFromBD.setName(vacancyDto.getName());
        vacancyEntityFromBD.setPosition(vacancyDto.getPosition());
        vacancyEntityFromBD.setSalary(vacancyDto.getSalary());
        vacancyEntityFromBD.setCompanyName(vacancyDto.getCompanyName());

        if (vacancyEntityFromBD.getName() == null) {
            throw new RuntimeException("VacancyService -> Name is empty");
        }

        if (vacancyEntityFromBD.getPosition() == null) {
            throw new RuntimeException("VacancyService -> Position is empty");
        }

        if (vacancyEntityFromBD.getCompanyName() == null) {
            throw new RuntimeException("VacancyService -> Company name is empty");
        }

        VacancyEntity updatedVacancy = vacancyRepository.update(vacancyEntityFromBD);
        System.out.println("VacancyService -> Updated vacancy: " + vacancyEntityFromBD);

        return vacancyMapperStruct.vacancyEntityToVacancyFullDto(updatedVacancy);
    }

    public VacancyFullDto create(VacancyCreateDto vacancyCreateDto) {

        VacancyEntity vacancyEntity = vacancyMapperStruct.vacancyCreateDtoToVacancyEntity(vacancyCreateDto);
        vacancyEntity.setUser(userRepository.findById(vacancyCreateDto.getUserId()));

        if (vacancyEntity.getName() == null) {
            throw new RuntimeException("VacancyService -> Name is empty");
        }

        if (vacancyEntity.getPosition() == null) {
            throw new RuntimeException("VacancyService -> Position is empty");
        }

        if (vacancyEntity.getCompanyName() == null) {
            throw new RuntimeException("VacancyService -> Company name is empty");
        }

        VacancyEntity createdVacancy = vacancyRepository.create(vacancyEntity);
        System.out.println("VacancyService -> Created user: " + createdVacancy);
        return vacancyMapper.mapVacancyFullDto(createdVacancy);
    }

    public String deleteById(int id) {
        VacancyEntity vacancyToDelete =  vacancyRepository.findById(id);
        if(vacancyToDelete == null) {
            throw new RuntimeException("VacancyService -> Vacancy was not found by id " + id);
        }

        List <InterviewEntity> interviewsOfVacancy = vacancyToDelete.getInterviews();

        for(InterviewEntity interview : interviewsOfVacancy) {
            interviewRepository.deleteById(interview.getId());
        }

        vacancyRepository.deleteById(id);
        System.out.println("VacancyService -> vacancy user: " + id + " was deleted");

        return "VacancyService -> vacancy: " + id + " was deleted";
    }

}
