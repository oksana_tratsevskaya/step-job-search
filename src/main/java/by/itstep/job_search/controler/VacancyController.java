package by.itstep.job_search.controler;

import by.itstep.job_search.dto.vacansyDto.VacancyCreateDto;
import by.itstep.job_search.dto.vacansyDto.VacancyFullDto;
import by.itstep.job_search.dto.vacansyDto.VacancyDto;
import by.itstep.job_search.service.VacancyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class VacancyController {


    private VacancyService vacancyService;

    @Autowired
    VacancyController (VacancyService vacancyService) {
        this.vacancyService = vacancyService;
    }

    @ResponseBody
    @RequestMapping(value = "/vacancy", method = RequestMethod.GET)
    public List<VacancyDto> findAll () {
        return vacancyService.findAll();
    }

    @ResponseBody
    @RequestMapping(value = "/vacancy/{id}", method = RequestMethod.GET)
    public VacancyFullDto findById(@PathVariable int id) {
        return vacancyService.findById(id);
    }

    @ResponseBody
    @RequestMapping(value = "/vacancy", method = RequestMethod.POST)
    public VacancyFullDto create(@Valid @RequestBody VacancyCreateDto vacancyCreateDto) {
        return vacancyService.create(vacancyCreateDto);
    }

    @ResponseBody
    @RequestMapping(value = "/vacancy", method = RequestMethod.PUT)
    public VacancyFullDto update(@Valid @RequestBody VacancyDto vacancyDto) {
        return vacancyService.update(vacancyDto);
    }

    @ResponseBody
    @RequestMapping(value = "/vacancy/{id}", method = RequestMethod.DELETE)
    public String delete (@PathVariable int id) {
        return vacancyService.deleteById(id);
    }
}
