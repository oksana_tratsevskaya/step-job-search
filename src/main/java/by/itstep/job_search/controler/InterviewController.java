package by.itstep.job_search.controler;

import by.itstep.job_search.dto.inerviewDto.InterviewCreateDto;
import by.itstep.job_search.dto.inerviewDto.InterviewFullDto;
import by.itstep.job_search.dto.inerviewDto.InterviewShortDto;
import by.itstep.job_search.dto.inerviewDto.InterviewUpdateDto;
import by.itstep.job_search.service.InterviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class InterviewController {

    private InterviewService interviewService;

    @Autowired
    public InterviewController (InterviewService interviewService) {
        this.interviewService = interviewService;
    }

    @ResponseBody
    @RequestMapping(value = "/interview", method = RequestMethod.GET)
    public List<InterviewShortDto> findAll() {
        return interviewService.findAll();
    }

    @ResponseBody
    @RequestMapping(value = "/interview/{id}", method = RequestMethod.GET)
    public InterviewFullDto findById(@PathVariable int id) {
        return interviewService.findById(id);
    }

    @ResponseBody
    @RequestMapping(value = "/interview", method = RequestMethod.POST)
    public InterviewFullDto create(@Valid @RequestBody InterviewCreateDto createDto) {
        return interviewService.create(createDto);
    }

    @ResponseBody
    @RequestMapping(value = "/interview", method = RequestMethod.PUT)
    public InterviewFullDto update(@Valid @RequestBody InterviewUpdateDto updateDto) {
        return interviewService.update(updateDto);
    }

    @ResponseBody
    @RequestMapping(value = "/interview/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id) {
        interviewService.deleteById(id);
    }
}
