package by.itstep.job_search.controler;

import by.itstep.job_search.dto.userDto.UserCreateDto;
import by.itstep.job_search.dto.userDto.UserDto;
import by.itstep.job_search.dto.userDto.UserShortDto;
import by.itstep.job_search.dto.userDto.UserUpdateDto;
import by.itstep.job_search.entity.UserEntity;
import by.itstep.job_search.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@Controller
public class UserController {

    @Autowired
    private UserService userService;


    public UserController(UserService userService) {
        this.userService = userService;
    }

    @ResponseBody
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<UserShortDto> findAllUser() {
        return  userService.findAll();
    }

    @ResponseBody
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public UserDto findById(@PathVariable int id) {
        return userService.findById(id);
    }

    @ResponseBody
    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public UserDto create(@Valid @RequestBody UserCreateDto userToCreate) {
        return userService.create(userToCreate);
    }

    @ResponseBody
    @RequestMapping(value = "/users", method = RequestMethod.PUT)
    public UserShortDto update(@Valid  @RequestBody UserUpdateDto userUpdateDto) {
        return userService.update(userUpdateDto);
    }

    @ResponseBody
    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable int id) {
        return userService.deleteById(id);
    }
}
