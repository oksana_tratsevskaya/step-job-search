package by.itstep.job_search.entity;

import by.itstep.job_search.enam.StatusOfInterview;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Data
@Table(name = "interview")
public class InterviewEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private StatusOfInterview status;

    @Column(name = "date")
    private Date date;

    @ManyToOne
    @JoinColumn(name = "vacancy_id")
    private VacancyEntity vacancy;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

}
