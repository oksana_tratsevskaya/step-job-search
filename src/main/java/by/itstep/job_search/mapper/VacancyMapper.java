package by.itstep.job_search.mapper;

import by.itstep.job_search.dto.vacansyDto.VacancyCreateDto;
import by.itstep.job_search.dto.vacansyDto.VacancyDto;
import by.itstep.job_search.dto.vacansyDto.VacancyFullDto;
import by.itstep.job_search.entity.VacancyEntity;
import by.itstep.job_search.repository.interfaceOfRepository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class VacancyMapper {

    @Autowired
    UserMapper userMapper;

    public List<VacancyDto> mapVacanciesDto(List<VacancyEntity> vacancyEntities) {
        List<VacancyDto> vacanciesDto = new ArrayList<>();

        for (VacancyEntity vacancyEntity :vacancyEntities) {
            vacanciesDto.add(mapVacancyDto(vacancyEntity));
        }

        return vacanciesDto;
    }

    public VacancyDto mapVacancyDto(VacancyEntity vacancyEntity) {
        return VacancyDto.builder()
                .id(vacancyEntity.getId())
                .name(vacancyEntity.getName())
                .position(vacancyEntity.getPosition())
                .salary(vacancyEntity.getSalary())
                .companyName(vacancyEntity.getCompanyName())
                .build();
    }

    public VacancyFullDto mapVacancyFullDto(VacancyEntity vacancyEntity) {
        return VacancyFullDto.builder()
                .id(vacancyEntity.getId())
                .name(vacancyEntity.getName())
                .position(vacancyEntity.getPosition())
                .salary(vacancyEntity.getSalary())
                .companyName(vacancyEntity.getCompanyName())
                .userId(userMapper.mapUserDto(vacancyEntity.getUser()))
                .build();
    }

    public VacancyEntity mapCreate(VacancyCreateDto vacancyCreateDto) {
        VacancyEntity vacancyEntity = new VacancyEntity();
        vacancyEntity.setName(vacancyCreateDto.getName());
        vacancyEntity.setPosition(vacancyCreateDto.getPosition());
        vacancyEntity.setSalary(vacancyCreateDto.getSalary());
        vacancyEntity.setCompanyName(vacancyCreateDto.getCompanyName());

        return vacancyEntity;
    }
}
