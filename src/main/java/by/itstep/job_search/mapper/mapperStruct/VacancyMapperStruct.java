package by.itstep.job_search.mapper.mapperStruct;

import by.itstep.job_search.dto.vacansyDto.VacancyCreateDto;
import by.itstep.job_search.dto.vacansyDto.VacancyDto;
import by.itstep.job_search.dto.vacansyDto.VacancyFullDto;
import by.itstep.job_search.entity.VacancyEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface VacancyMapperStruct {

    List<VacancyDto> vacanciesEntityToVacanciesDto(List<VacancyEntity> vacancyEntities);
    VacancyDto vacancyEntityToVacancyDto(VacancyEntity vacancyEntity);
    VacancyFullDto vacancyEntityToVacancyFullDto(VacancyEntity vacancyEntity);
    VacancyEntity vacancyCreateDtoToVacancyEntity(VacancyCreateDto vacancyCreateDto);

}
