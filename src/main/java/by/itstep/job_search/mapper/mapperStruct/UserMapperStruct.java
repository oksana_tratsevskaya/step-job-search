package by.itstep.job_search.mapper.mapperStruct;

import by.itstep.job_search.dto.userDto.UserCreateDto;
import by.itstep.job_search.dto.userDto.UserDto;
import by.itstep.job_search.dto.userDto.UserShortDto;
import by.itstep.job_search.entity.UserEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface UserMapperStruct {

    List<UserShortDto> userEntitiesToUsersShortDto(List<UserEntity> userEntities);
    UserShortDto userEntitiesToUserShortDto(UserEntity userEntity);
    UserDto userEntityToUserDto(UserEntity userEntity);
    UserEntity userDtoToUserEntity(UserCreateDto userCreateDto);

}
