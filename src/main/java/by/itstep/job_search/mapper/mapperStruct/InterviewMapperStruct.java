package by.itstep.job_search.mapper.mapperStruct;

import by.itstep.job_search.dto.inerviewDto.InterviewCreateDto;
import by.itstep.job_search.dto.inerviewDto.InterviewFullDto;
import by.itstep.job_search.dto.inerviewDto.InterviewShortDto;
import by.itstep.job_search.entity.InterviewEntity;
import org.mapstruct.Mapper;


import java.util.List;

@Mapper
public interface InterviewMapperStruct {

    List<InterviewShortDto> interviewEntitiesToInterviewShortDto(List <InterviewEntity> interviewEntities);
    InterviewShortDto interviewEntityToInterviewShortDto(InterviewEntity interviewEntity);
    InterviewFullDto interviewEntityToInterviewFullDto(InterviewEntity interviewEntity);
    InterviewEntity interviewCreateDtoToInterviewEntity(InterviewCreateDto interviewCreateDto);


}
