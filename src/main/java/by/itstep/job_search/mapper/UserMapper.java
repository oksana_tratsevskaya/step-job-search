package by.itstep.job_search.mapper;

import by.itstep.job_search.dto.userDto.UserCreateDto;
import by.itstep.job_search.dto.userDto.UserDto;
import by.itstep.job_search.dto.userDto.UserShortDto;
import by.itstep.job_search.dto.userDto.UserUpdateDto;
import by.itstep.job_search.entity.UserEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserMapper {

    public List<UserShortDto> mapListShortDto(List<UserEntity> usersEntity) {
        List<UserShortDto> usersDto = new ArrayList<>();

        for (UserEntity userEntity : usersEntity) {
            usersDto.add(mapShortDto(userEntity));
        }

        return usersDto;
    }

    public UserShortDto mapShortDto(UserEntity userEntity) {
        return UserShortDto.builder()
                .id(userEntity.getId())
                .firstName(userEntity.getFirstName())
                .lastName(userEntity.getLastName())
                .yearsOfExperience(userEntity.getYearsOfExperience())
                .position(userEntity.getPosition())
                .build();
    }

    public UserDto mapUserDto(UserEntity userEntity) {
        return UserDto.builder()
                .id(userEntity.getId())
                .firstName(userEntity.getFirstName())
                .lastName(userEntity.getLastName())
                .yearsOfExperience(userEntity.getYearsOfExperience())
                .position(userEntity.getPosition())
                .phone(userEntity.getPhone())
                .email(userEntity.getEmail())
                .build();
    }

    public UserEntity mapCreate(UserCreateDto userCreateDto) {
        UserEntity userEntity = new UserEntity();

        userEntity.setFirstName(userCreateDto.getFirstName());
        userEntity.setLastName(userCreateDto.getLastName());
        userEntity.setYearsOfExperience(userCreateDto.getYearsOfExperience());
        userEntity.setPosition(userCreateDto.getPosition());
        userEntity.setPhone(userCreateDto.getPhone());
        userEntity.setEmail(userCreateDto.getEmail());
        userEntity.setRole(userCreateDto.getRole());
        userEntity.setPassword(userCreateDto.getPassword());

        return userEntity;
    }
}
