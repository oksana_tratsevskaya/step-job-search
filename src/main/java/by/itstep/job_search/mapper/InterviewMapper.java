package by.itstep.job_search.mapper;

import by.itstep.job_search.dto.inerviewDto.InterviewCreateDto;
import by.itstep.job_search.dto.inerviewDto.InterviewFullDto;
import by.itstep.job_search.dto.inerviewDto.InterviewShortDto;
import by.itstep.job_search.entity.InterviewEntity;
import by.itstep.job_search.repository.interfaceOfRepository.UserRepository;
import by.itstep.job_search.repository.interfaceOfRepository.VacancyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class InterviewMapper {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private VacancyMapper vacancyMapper;

    public List<InterviewShortDto> mapShortDtos(List <InterviewEntity> interviewEntities) {
        List<InterviewShortDto> interviewsShortDto = new ArrayList<>();

        for (InterviewEntity interviewEntity : interviewEntities) {
            interviewsShortDto.add(mapShortDto(interviewEntity));
        }

        return interviewsShortDto;
    }

    public InterviewShortDto mapShortDto(InterviewEntity interviewEntity ) {
        return InterviewShortDto.builder()
                .id(interviewEntity.getId())
                .status(interviewEntity.getStatus())
                .date(interviewEntity.getDate())
                .build();
    }

    public InterviewFullDto mapFullDto(InterviewEntity interviewEntity) {
        return InterviewFullDto.builder()
                .id(interviewEntity.getId())
                .status(interviewEntity.getStatus())
                .date(interviewEntity.getDate())
                .vacancy(vacancyMapper.mapVacancyDto(interviewEntity.getVacancy()))
                .user(userMapper.mapUserDto(interviewEntity.getUser()))
                .build();
    }

    public InterviewEntity mapCreateEntity(InterviewCreateDto createDto) {
        InterviewEntity interviewEntity = new InterviewEntity();

        interviewEntity.setDate(createDto.getDate());


        return interviewEntity;
    }
}
