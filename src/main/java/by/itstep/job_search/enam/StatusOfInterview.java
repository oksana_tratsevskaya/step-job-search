package by.itstep.job_search.enam;

public enum StatusOfInterview {

    REQUESTED,
    ACCEPTED,
    FINISHED,
    REJECTED

}
