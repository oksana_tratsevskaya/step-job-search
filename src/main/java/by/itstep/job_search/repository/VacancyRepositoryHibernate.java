package by.itstep.job_search.repository;

import by.itstep.job_search.entity.VacancyEntity;
import by.itstep.job_search.repository.interfaceOfRepository.VacancyRepository;
import by.itstep.job_search.utils.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class VacancyRepositoryHibernate implements VacancyRepository {

    @Override
    public VacancyEntity findById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        VacancyEntity vacancy = em.find(VacancyEntity.class, id);

        if(vacancy != null){
            Hibernate.initialize(vacancy.getInterviews());
        }

        em.getTransaction().commit();
        em.close();

        return vacancy;
    }

    @Override
    public List<VacancyEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        List<VacancyEntity> foundVacancy = em.createNativeQuery("SELECT * FROM vacancy", VacancyEntity.class).getResultList();

        em.getTransaction().commit();
        em.close();

        return foundVacancy;
    }

    @Override
    public VacancyEntity create(VacancyEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(entity);

        em.getTransaction().commit();
        em.close();

        return entity;
    }

    @Override
    public VacancyEntity update(VacancyEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(entity);
        em.getTransaction().commit();
        em.close();

        return entity;
    }

    @Override
    public void deleteById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        VacancyEntity vacancyToRemove = em.find(VacancyEntity.class, id);
        em.remove(vacancyToRemove);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM vacancy").executeUpdate();
        
        em.getTransaction().commit();
        em.close();
    }
}
