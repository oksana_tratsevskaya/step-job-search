package by.itstep.job_search.repository;

import by.itstep.job_search.entity.InterviewEntity;
import by.itstep.job_search.repository.interfaceOfRepository.InterviewRepository;
import by.itstep.job_search.utils.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class InterviewRepositoryHibernate implements InterviewRepository {

    @Override
    public InterviewEntity findById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        InterviewEntity interview = em.find(InterviewEntity.class, id);

        em.getTransaction().commit();
        em.close();

        return interview;
    }

    @Override
    public List<InterviewEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        List <InterviewEntity> foundInterview = em.createNativeQuery("SELECT * FROM interview", InterviewEntity.class).getResultList();

        em.getTransaction().commit();
        em.close();

        return foundInterview;
    }

    @Override
    public InterviewEntity create(InterviewEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(entity);

        em.getTransaction().commit();
        em.close();

        return entity;
    }

    @Override
    public InterviewEntity update(InterviewEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(entity);

        em.getTransaction().commit();
        em.close();

        return entity;
    }

    @Override
    public void deleteById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        InterviewEntity interviewToRemove = em.find(InterviewEntity.class, id);
        em.remove(interviewToRemove);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM interview").executeUpdate();
        
        em.getTransaction().commit();
        em.close();
    }
}
