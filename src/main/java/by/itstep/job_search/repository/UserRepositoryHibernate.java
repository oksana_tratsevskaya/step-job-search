package by.itstep.job_search.repository;

import by.itstep.job_search.entity.UserEntity;
import by.itstep.job_search.repository.interfaceOfRepository.UserRepository;
import by.itstep.job_search.utils.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class UserRepositoryHibernate implements UserRepository {

    @Override
    public UserEntity findById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        UserEntity foundUser = em.find(UserEntity.class, id);

        if(foundUser != null) {
            Hibernate.initialize(foundUser.getInterviews());
            Hibernate.initialize(foundUser.getVacancies());
        }

        em.getTransaction().commit();
        em.close();

        return foundUser;
    }

    @Override
    public List<UserEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        List<UserEntity> foundAllUsers = em.createNativeQuery("SELECT * FROM users", UserEntity.class).getResultList();

        for(UserEntity user : foundAllUsers) {
            Hibernate.initialize(user.getInterviews());
        }

        em.getTransaction().commit();
        em.close();

        return foundAllUsers;
    }

    @Override
    public UserEntity create(UserEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(entity);

        em.getTransaction().commit();
        em.close();

        return entity;
    }

    @Override
    public UserEntity update(UserEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(entity);

        em.getTransaction().commit();
        em.close();

        return entity;
    }

    @Override
    public void deleteById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        UserEntity userToRemove = em.find(UserEntity.class, id);
        em.remove(userToRemove);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM users").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
