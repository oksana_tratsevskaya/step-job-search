package by.itstep.job_search.mapper;

import by.itstep.job_search.dto.userDto.UserCreateDto;
import by.itstep.job_search.dto.userDto.UserDto;
import by.itstep.job_search.dto.userDto.UserShortDto;
import by.itstep.job_search.entity.UserEntity;
import by.itstep.job_search.util.DBCleaner;
import by.itstep.job_search.util.EntityUtil;
import org.apache.catalina.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class UserMapperTest {

    UserMapper userMapper = new UserMapper();

    @Test
    public void createUserShortDto_happyPath() {

        //give
        UserEntity userEntity = EntityUtil.prepareUser();

        //when
        UserShortDto userShortDto = userMapper.mapShortDto(userEntity);

        //then
        Assertions.assertEquals(userShortDto.getId(), userEntity.getId());
        Assertions.assertEquals(userShortDto.getFirstName(), userEntity.getFirstName());
        Assertions.assertEquals(userShortDto.getLastName(), userEntity.getLastName());
        Assertions.assertEquals(userShortDto.getYearsOfExperience(), userEntity.getYearsOfExperience());
        Assertions.assertEquals(userShortDto.getPosition(), userEntity.getPosition());

    }

    @Test
    public void createUserShortListDto_happyPath() {
        //give
        List<UserEntity> userEntities = new ArrayList<>();
        userEntities.add(EntityUtil.prepareUser());
        userEntities.add(EntityUtil.prepareUser());
        userEntities.add(EntityUtil.prepareUser());
        userEntities.add(EntityUtil.prepareUser());

        //when
        List<UserShortDto> usersShortDto = userMapper.mapListShortDto(userEntities);

        //then
        Assertions.assertEquals(usersShortDto.size(), 4);
    }

    @Test
    public void createUserDto_happyPath() {

        //give
        UserEntity userEntity = EntityUtil.prepareUser();

        //when
        UserDto userDto = userMapper.mapUserDto(userEntity);

        //then
        Assertions.assertEquals(userDto.getId(), userEntity.getId());
        Assertions.assertEquals(userDto.getFirstName(), userEntity.getFirstName());
        Assertions.assertEquals(userDto.getLastName(), userEntity.getLastName());
        Assertions.assertEquals(userDto.getYearsOfExperience(), userEntity.getYearsOfExperience());
        Assertions.assertEquals(userDto.getPosition(), userEntity.getPosition());
        Assertions.assertEquals(userDto.getPhone(), userEntity.getPhone());
        Assertions.assertEquals(userDto.getEmail(), userEntity.getEmail());

    }

    @Test
    public void mapCreate_happyPath() {

        //give
        UserCreateDto userCreateDto = EntityUtil.prepareUserCreateDto();

        //when
        UserEntity userEntity = userMapper.mapCreate(userCreateDto);

        //then
        Assertions.assertEquals(userEntity.getFirstName(), userCreateDto.getFirstName());
        Assertions.assertEquals(userEntity.getLastName(), userCreateDto.getLastName());
        Assertions.assertEquals(userEntity.getYearsOfExperience(), userCreateDto.getYearsOfExperience());
        Assertions.assertEquals(userEntity.getPosition(), userCreateDto.getPosition());
        Assertions.assertEquals(userEntity.getPhone(), userCreateDto.getPhone());
        Assertions.assertEquals(userEntity.getEmail(), userCreateDto.getEmail());
        Assertions.assertEquals(userEntity.getRole(), userCreateDto.getRole());
        Assertions.assertEquals(userEntity.getPassword(), userCreateDto.getPassword());

    }
}
