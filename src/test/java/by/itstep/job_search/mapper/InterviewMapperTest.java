package by.itstep.job_search.mapper;

import by.itstep.job_search.dto.inerviewDto.InterviewFullDto;
import by.itstep.job_search.dto.inerviewDto.InterviewShortDto;
import by.itstep.job_search.dto.userDto.UserShortDto;
import by.itstep.job_search.entity.InterviewEntity;
import by.itstep.job_search.entity.UserEntity;
import by.itstep.job_search.util.EntityUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class InterviewMapperTest {


    InterviewMapper interviewMapper = new InterviewMapper();


    @Test
    public void createUserShortDto_happyPath() {

        //give
        InterviewEntity interviewEntity = EntityUtil.prepareInterview(EntityUtil.prepareUser(),
                EntityUtil.prepareVacancy(EntityUtil.prepareUser()));

        //when
        InterviewShortDto interviewShortDto = interviewMapper.mapShortDto(interviewEntity);

        //then
        Assertions.assertEquals(interviewShortDto.getId(), interviewEntity.getId());
        Assertions.assertEquals(interviewShortDto.getStatus(), interviewEntity.getStatus());
        Assertions.assertEquals(interviewShortDto.getDate(), interviewEntity.getDate());
   }

    @Test
    public void createUserShortListDto_happyPath() {
        //give
        List<InterviewEntity> interviewEntities = new ArrayList<>();
        interviewEntities.add(EntityUtil.prepareInterview(EntityUtil.prepareUser(),
                EntityUtil.prepareVacancy(EntityUtil.prepareUser())));
        interviewEntities.add(EntityUtil.prepareInterview(EntityUtil.prepareUser(),
                EntityUtil.prepareVacancy(EntityUtil.prepareUser())));
        interviewEntities.add(EntityUtil.prepareInterview(EntityUtil.prepareUser(),
                EntityUtil.prepareVacancy(EntityUtil.prepareUser())));
        interviewEntities.add(EntityUtil.prepareInterview(EntityUtil.prepareUser(),
                EntityUtil.prepareVacancy(EntityUtil.prepareUser())));

        //when
        List<InterviewShortDto> interviewShortDtos = interviewMapper.mapShortDtos(interviewEntities);

        //then
        Assertions.assertEquals(interviewShortDtos.size(), 4);
    }

//    @Test
//    public void createFullDto() {
//        //give
//        InterviewEntity interviewEntity = EntityUtil.prepareInterview(EntityUtil.prepareUser(),
//                EntityUtil.prepareVacancy(EntityUtil.prepareUser()));
//
//        //when
//        InterviewFullDto interviewFullDto = interviewMapper.mapFullDto(interviewEntity);
//
//        //then
//        Assertions.assertEquals(interviewFullDto.getId(), interviewEntity.getId());
//        Assertions.assertEquals(interviewFullDto.getStatus(), interviewEntity.getStatus());
//        Assertions.assertEquals(interviewFullDto.getDate(), interviewEntity.getDate());
//        //Assertions.assertEquals(interviewFullDto.getVacancy().getId(), interviewEntity.getVacancy().getId());
//        //Assertions.assertEquals(interviewFullDto.getUser().getId(), interviewEntity.getUser().getId());
//    }


}
