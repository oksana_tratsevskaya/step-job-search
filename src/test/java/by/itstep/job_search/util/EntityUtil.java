package by.itstep.job_search.util;

import by.itstep.job_search.dto.userDto.UserCreateDto;
import by.itstep.job_search.dto.userDto.UserUpdateDto;
import by.itstep.job_search.enam.RoleOfUser;
import by.itstep.job_search.enam.StatusOfInterview;
import by.itstep.job_search.entity.InterviewEntity;
import by.itstep.job_search.entity.UserEntity;
import by.itstep.job_search.entity.VacancyEntity;
import by.itstep.job_search.service.UserService;
import com.github.javafaker.Faker;
import org.springframework.stereotype.Component;

import javax.swing.text.html.parser.Entity;
import java.sql.Date;


public class EntityUtil {

    private static Faker faker = new Faker();

    public static UserEntity prepareUser() {
        UserEntity user = new UserEntity();
        user.setRole(RoleOfUser.HR);
        user.setFirstName(faker.name().firstName());
        user.setLastName(faker.name().lastName());
        user.setPhone(faker.phoneNumber().phoneNumber());
        user.setEmail(faker.internet().emailAddress());
        user.setPassword(faker.beer().name());
        user.setYearsOfExperience(faker.number().numberBetween(0, 20));
        user.setPosition(faker.company().profession());

        return user;
    }

    public static VacancyEntity prepareVacancy(UserEntity user) {
        VacancyEntity vacancy = new VacancyEntity();
        vacancy.setName(faker.cat().name());
        vacancy.setPosition(faker.company().profession());
        vacancy.setSalary(faker.number().numberBetween(0, 50000));
        vacancy.setCompanyName(faker.company().name());
        vacancy.setUser(user);

        return vacancy;
    }

    public static InterviewEntity prepareInterview(UserEntity user, VacancyEntity vacancy) {
        InterviewEntity interview = new InterviewEntity();
        interview.setStatus(StatusOfInterview.ACCEPTED);
        interview.setDate(new Date(2022, 02, 14));
        interview.setUser(user);
        interview.setVacancy(vacancy);

        return interview;
    }

    public static UserCreateDto prepareUserCreateDto() {
        return UserCreateDto.builder()
                .firstName(faker.name().firstName())
                .lastName(faker.name().lastName())
                .yearsOfExperience(faker.number().numberBetween(0, 20))
                .position(faker.company().profession())
                .phone(faker.phoneNumber().phoneNumber())
                .email(faker.internet().emailAddress())
                .role(RoleOfUser.CANDIDATE)
                .password(faker.internet().password())
                .build();
    }

    public static UserUpdateDto prepareUserUpdateDto(UserEntity userEntity) {
        return UserUpdateDto.builder()
                .id(userEntity.getId())
                .firstName(faker.name().firstName())
                .lastName(faker.name().lastName())
                .yearsOfExperience(faker.number().numberBetween(0, 20))
                .position(faker.company().profession())
                .phone(faker.phoneNumber().phoneNumber())
                .role(RoleOfUser.CANDIDATE)
                .build();
    }
}