package by.itstep.job_search.util;

import by.itstep.job_search.repository.interfaceOfRepository.InterviewRepository;
import by.itstep.job_search.repository.interfaceOfRepository.UserRepository;
import by.itstep.job_search.repository.interfaceOfRepository.VacancyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DBCleaner {

    @Autowired
    private InterviewRepository interviewRepository;

    @Autowired
    private VacancyRepository vacancyRepository;

    @Autowired
    private UserRepository userRepository;

    public void clean() {
        interviewRepository.deleteAll();
        vacancyRepository.deleteAll();
        userRepository.deleteAll();
    }
}
