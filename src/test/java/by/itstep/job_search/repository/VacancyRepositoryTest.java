package by.itstep.job_search.repository;

import by.itstep.job_search.entity.UserEntity;
import by.itstep.job_search.entity.VacancyEntity;
import by.itstep.job_search.repository.interfaceOfRepository.UserRepository;
import by.itstep.job_search.repository.interfaceOfRepository.VacancyRepository;
import by.itstep.job_search.util.DBCleaner;
import by.itstep.job_search.util.EntityUtil;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class VacancyRepositoryTest {

    @Autowired
    VacancyRepository vacancyRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    DBCleaner dbCleaner;

    @BeforeEach
    public void setUp() {
        dbCleaner.clean();
    }

    @AfterEach
    public void shutDown() {
        dbCleaner.clean();
    }

    @Test
    public void create_happyPass() {
        //given
        UserEntity user = userRepository.create(EntityUtil.prepareUser());
        VacancyEntity vacancy = EntityUtil.prepareVacancy(user);

        //when
        VacancyEntity createdVacancy = vacancyRepository.create(vacancy);

        //then
        Assertions.assertNotNull(createdVacancy.getId());
        VacancyEntity foundVacancy = vacancyRepository.findById(createdVacancy.getId());
        Assertions.assertEquals(foundVacancy.getName(), vacancy.getName());
        Assertions.assertEquals(foundVacancy.getCompanyName(), vacancy.getCompanyName());
        Assertions.assertEquals(foundVacancy.getPosition(), vacancy.getPosition());
        Assertions.assertEquals(foundVacancy.getSalary(), vacancy.getSalary());
        Assertions.assertEquals(foundVacancy.getUser().getId(), vacancy.getUser().getId());
    }

    @Test
    public void update_happyPass() {
        //give
        Faker faker = new Faker();
        UserEntity user = userRepository.create(EntityUtil.prepareUser());
        VacancyEntity existVacancy = vacancyRepository.create(EntityUtil.prepareVacancy(user));
        existVacancy.setName(faker.cat().name());
        existVacancy.setPosition(faker.company().profession());
        existVacancy.setSalary(faker.number().numberBetween(0, 50000));
        existVacancy.setCompanyName(faker.company().name());
        existVacancy.setUser(user);

        //when
        VacancyEntity updatedVacancy = vacancyRepository.update(existVacancy);

        //then
        Assertions.assertEquals(updatedVacancy.getId(),existVacancy.getId());
        VacancyEntity foundVacancy = vacancyRepository.findById(updatedVacancy.getId());
        Assertions.assertEquals(foundVacancy.getName(), existVacancy.getName());
        Assertions.assertEquals(foundVacancy.getCompanyName(), existVacancy.getCompanyName());
        Assertions.assertEquals(foundVacancy.getPosition(), existVacancy.getPosition());
        Assertions.assertEquals(foundVacancy.getSalary(), existVacancy.getSalary());
        Assertions.assertEquals(foundVacancy.getUser().getId(), existVacancy.getUser().getId());
    }

    @Test
    public void findById_happyPass() {
        //give
        UserEntity user = userRepository.create(EntityUtil.prepareUser());
        VacancyEntity vacancy = vacancyRepository.create(EntityUtil.prepareVacancy(user));

        //when
        VacancyEntity foundedVacancy = vacancyRepository.findById(vacancy.getId());

        //then
        Assertions.assertEquals(vacancy.getId(), foundedVacancy.getId());

    }

    @Test
    public void findAll_happyPass() {
        //give
        UserEntity user = userRepository.create(EntityUtil.prepareUser());
        vacancyRepository.create(EntityUtil.prepareVacancy(user));
        vacancyRepository.create(EntityUtil.prepareVacancy(user));
        vacancyRepository.create(EntityUtil.prepareVacancy(user));

        //when
        List<VacancyEntity> vacancies = vacancyRepository.findAll();

        //then
        Assertions.assertEquals(3, vacancies.size());
    }

    @Test
    public void delete_happyPass() {
        //give
        UserEntity user = userRepository.create(EntityUtil.prepareUser());
        VacancyEntity vacancyToDelete = vacancyRepository.create(EntityUtil.prepareVacancy(user));

        //when
        vacancyRepository.deleteById(vacancyToDelete.getId());

        //then
        VacancyEntity foundVacancy = vacancyRepository.findById(vacancyToDelete.getId());
        Assertions.assertNull(foundVacancy);
    }
}
