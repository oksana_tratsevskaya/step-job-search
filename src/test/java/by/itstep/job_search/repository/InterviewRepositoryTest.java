package by.itstep.job_search.repository;

import by.itstep.job_search.enam.StatusOfInterview;
import by.itstep.job_search.entity.InterviewEntity;
import by.itstep.job_search.entity.UserEntity;
import by.itstep.job_search.entity.VacancyEntity;
import by.itstep.job_search.repository.interfaceOfRepository.InterviewRepository;
import by.itstep.job_search.repository.interfaceOfRepository.UserRepository;
import by.itstep.job_search.repository.interfaceOfRepository.VacancyRepository;
import by.itstep.job_search.util.DBCleaner;
import by.itstep.job_search.util.EntityUtil;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Date;
import java.util.List;

@SpringBootTest
public class InterviewRepositoryTest {

    @Autowired
    InterviewRepository interviewRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    VacancyRepository vacancyRepository;

    @Autowired
    DBCleaner dbCleaner;

    @BeforeEach
    public void setUp() {
        dbCleaner.clean();
    }

    @AfterEach
    public void shutDown() {
        dbCleaner.clean();
    }

    @Test
    public void create_happyPath() {
        //given
        UserEntity user = userRepository.create(EntityUtil.prepareUser());
        VacancyEntity vacancy = vacancyRepository.create(EntityUtil.prepareVacancy(user));
        InterviewEntity interview = EntityUtil.prepareInterview(user, vacancy);

        //when
        InterviewEntity createdInterview = interviewRepository.create(interview);

        //then
        Assertions.assertNotNull(createdInterview.getId());
        InterviewEntity foundInterview = interviewRepository.findById(createdInterview.getId());
        Assertions.assertEquals(foundInterview.getDate(), interview.getDate());
        Assertions.assertEquals(foundInterview.getStatus(), interview.getStatus());
        Assertions.assertEquals(foundInterview.getVacancy().getId(), interview.getVacancy().getId());
        Assertions.assertEquals(foundInterview.getUser().getId(), interview.getUser().getId());
    }

    @Test
    public void update_happyPass() {
        //give
        Faker faker = new Faker();
        UserEntity user = userRepository.create(EntityUtil.prepareUser());
        VacancyEntity vacancy = vacancyRepository.create(EntityUtil.prepareVacancy(user));
        InterviewEntity existInterview = interviewRepository.create(EntityUtil.prepareInterview(user, vacancy));
        existInterview.setStatus(StatusOfInterview.FINISHED);
        existInterview.setDate(new Date(2022, 02, 14));
        existInterview.setVacancy(vacancy);
        existInterview.setUser(user);

        //when
        InterviewEntity updatedInterview = interviewRepository.update(existInterview);

        //then
        Assertions.assertEquals(updatedInterview.getId(),existInterview.getId());
        InterviewEntity foundInterview = interviewRepository.findById(updatedInterview.getId());
        Assertions.assertEquals(foundInterview.getStatus(), existInterview.getStatus());
        Assertions.assertEquals(foundInterview.getDate(), existInterview.getDate());
        Assertions.assertEquals(foundInterview.getVacancy(), existInterview.getVacancy());
        Assertions.assertEquals(foundInterview.getUser().getId(), existInterview.getUser().getId());
    }

    @Test
    public void findById_happyPass() {
        //give
        UserEntity user = userRepository.create(EntityUtil.prepareUser());
        VacancyEntity vacancy = vacancyRepository.create(EntityUtil.prepareVacancy(user));
        InterviewEntity interview = interviewRepository.create(EntityUtil.prepareInterview(user, vacancy));

        //when
        InterviewEntity foundedInterview = interviewRepository.findById(interview.getId());

        //then
        Assertions.assertEquals(interview.getId(), foundedInterview.getId());
    }

    @Test
    public void findAll_happyPass() {
        //give
        UserEntity user = userRepository.create(EntityUtil.prepareUser());
        VacancyEntity vacancy = vacancyRepository.create(EntityUtil.prepareVacancy(user));
        interviewRepository.create(EntityUtil.prepareInterview(user, vacancy));
        interviewRepository.create(EntityUtil.prepareInterview(user, vacancy));
        interviewRepository.create(EntityUtil.prepareInterview(user, vacancy));

        //when
        List<InterviewEntity> interviews = interviewRepository.findAll();

        //then
        Assertions.assertEquals(3, interviews.size());
    }

    @Test
    public void delete_happyPass() {
        //give
        UserEntity user = userRepository.create(EntityUtil.prepareUser());
        VacancyEntity vacancy = vacancyRepository.create(EntityUtil.prepareVacancy(user));
        InterviewEntity interviewToDelete = interviewRepository.create(EntityUtil.prepareInterview(user, vacancy));

        //when
        interviewRepository.deleteById(interviewToDelete.getId());

        //then
        InterviewEntity deletedInterview = interviewRepository.findById(interviewToDelete.getId());
        Assertions.assertNull(deletedInterview);
    }
}
