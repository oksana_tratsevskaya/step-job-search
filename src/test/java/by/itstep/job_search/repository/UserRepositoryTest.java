package by.itstep.job_search.repository;

import by.itstep.job_search.enam.RoleOfUser;
import by.itstep.job_search.entity.UserEntity;
import by.itstep.job_search.repository.interfaceOfRepository.UserRepository;
import by.itstep.job_search.util.DBCleaner;
import by.itstep.job_search.util.EntityUtil;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private  DBCleaner dbCleaner;

    @BeforeEach
    public void setUp() {
        dbCleaner.clean();
    }

    @AfterEach
    private void shotDown() {
       dbCleaner.clean();
    }

    @Test
    public void create_happyPath() {
        //given
        UserEntity user = EntityUtil.prepareUser();

        //when
        UserEntity createdUser = userRepository.create(user);

        //then
        Assertions.assertNotNull(createdUser.getId());
        UserEntity foundUser = userRepository.findById(createdUser.getId());
        Assertions.assertEquals(user.getRole(), foundUser.getRole());
        Assertions.assertEquals(user.getFirstName(), foundUser.getFirstName());
        Assertions.assertEquals(user.getLastName(), foundUser.getLastName());
        Assertions.assertEquals(user.getEmail(), foundUser.getEmail());
        Assertions.assertEquals(user.getPhone(), foundUser.getPhone());
        Assertions.assertEquals(user.getPassword(), foundUser.getPassword());
        Assertions.assertEquals(user.getYearsOfExperience(), foundUser.getYearsOfExperience());
        Assertions.assertEquals(user.getPosition(), foundUser.getPosition());
    }

    @Test
    public void findById_happyPass() {
        //given
        UserEntity user = userRepository.create(EntityUtil.prepareUser());

        //when
        UserEntity foundedUser = userRepository.findById(user.getId());

        //then
        Assertions.assertEquals(foundedUser.getId(), user.getId());
    }

    @Test
    public void findAll_happyPass() {
        //given
        userRepository.create(EntityUtil.prepareUser());
        userRepository.create(EntityUtil.prepareUser());
        userRepository.create(EntityUtil.prepareUser());

        //when
        List<UserEntity> users = userRepository.findAll();

        //then
        Assertions.assertEquals(3, users.size());
    }

    @Test
    public void update_happyPass() {
        //give
        Faker faker = new Faker();
        UserEntity existedUser = userRepository.create(EntityUtil.prepareUser());
        existedUser.setRole(RoleOfUser.CANDIDATE);
        existedUser.setFirstName(faker.name().firstName());
        existedUser.setLastName(faker.name().lastName());
        existedUser.setPhone(faker.phoneNumber().phoneNumber());
        existedUser.setEmail(faker.internet().emailAddress());
        existedUser.setPassword(faker.beer().name());
        existedUser.setYearsOfExperience(faker.number().numberBetween(0, 20));
        existedUser.setPosition(faker.company().profession());

        //when
        UserEntity updatedUser = userRepository.update(existedUser);

        //then
        Assertions.assertEquals(updatedUser.getId(), existedUser.getId());
        UserEntity foundEntity = userRepository.findById(updatedUser.getId());
        Assertions.assertEquals(foundEntity.getRole(), existedUser.getRole());
        Assertions.assertEquals(foundEntity.getFirstName(), existedUser.getFirstName());
        Assertions.assertEquals(foundEntity.getLastName(), existedUser.getLastName());
        Assertions.assertEquals(foundEntity.getEmail(), existedUser.getEmail());
        Assertions.assertEquals(foundEntity.getPhone(), existedUser.getPhone());
        Assertions.assertEquals(foundEntity.getPassword(), existedUser.getPassword());
        Assertions.assertEquals(foundEntity.getYearsOfExperience(), existedUser.getYearsOfExperience());
        Assertions.assertEquals(foundEntity.getPosition(), existedUser.getPosition());
    }

    @Test
    public void delete_happyPass() {
        //give
        UserEntity userForDelete = userRepository.create(EntityUtil.prepareUser());

        //when
        userRepository.deleteById(userForDelete.getId());

        //then
        UserEntity deletedUser = userRepository.findById(userForDelete.getId());
        Assertions.assertNull(deletedUser);
    }
}
