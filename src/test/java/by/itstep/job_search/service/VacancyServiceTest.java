package by.itstep.job_search.service;

import by.itstep.job_search.entity.InterviewEntity;
import by.itstep.job_search.entity.UserEntity;
import by.itstep.job_search.entity.VacancyEntity;
import by.itstep.job_search.repository.interfaceOfRepository.VacancyRepository;
import by.itstep.job_search.util.DBCleaner;
import by.itstep.job_search.util.EntityUtil;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class VacancyServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private InterviewService interviewService;

    @Autowired
    private VacancyService vacancyService;

    @Autowired
    private VacancyRepository vacancyRepository;

    @Autowired
    private DBCleaner dbCleaner;

//    @BeforeEach
//    public void setUp() {
//        dbCleaner.clean();
//    }
//
//    @AfterEach
//    public void shutDown() {
//        dbCleaner.clean();
//    }

//    @Test
//    public void create_happyPass() {
//        //given
//        UserEntity user = userService.create(EntityUtil.prepareUser());
//        VacancyEntity vacancy = EntityUtil.prepareVacancy(user);
//
//        //when
//        VacancyEntity createdVacancy = vacancyService.create(vacancy);
//
//        //then
//        Assertions.assertNotNull(createdVacancy.getId());
//        VacancyEntity foundVacancy = vacancyService.findById(createdVacancy.getId());
//        Assertions.assertEquals(foundVacancy.getName(), vacancy.getName());
//        Assertions.assertEquals(foundVacancy.getCompanyName(), vacancy.getCompanyName());
//        Assertions.assertEquals(foundVacancy.getPosition(), vacancy.getPosition());
//        Assertions.assertEquals(foundVacancy.getSalary(), vacancy.getSalary());
//        Assertions.assertEquals(foundVacancy.getUser().getId(), vacancy.getUser().getId());
//    }

//    @Test
//    public void update_happyPass() {
//        //give
//        Faker faker = new Faker();
//        UserEntity user = userService.create(EntityUtil.prepareUser());
//        VacancyEntity existVacancy = vacancyService.create(EntityUtil.prepareVacancy(user));
//        existVacancy.setName(faker.cat().name());
//        existVacancy.setPosition(faker.company().profession());
//        existVacancy.setSalary(faker.number().numberBetween(0, 50000));
//        existVacancy.setCompanyName(faker.company().name());
//        existVacancy.setUser(user);
//
//        //when
//        VacancyEntity updatedVacancy = vacancyService.update(existVacancy);
//
//        //then
//        Assertions.assertEquals(updatedVacancy.getId(),existVacancy.getId());
//        VacancyEntity foundVacancy = vacancyService.findById(updatedVacancy.getId());
//        Assertions.assertEquals(foundVacancy.getName(), existVacancy.getName());
//        Assertions.assertEquals(foundVacancy.getCompanyName(), existVacancy.getCompanyName());
//        Assertions.assertEquals(foundVacancy.getPosition(), existVacancy.getPosition());
//        Assertions.assertEquals(foundVacancy.getSalary(), existVacancy.getSalary());
//        Assertions.assertEquals(foundVacancy.getUser().getId(), existVacancy.getUser().getId());
//    }

//    @Test
//    public void findById_happyPass() {
//        //give
//        UserEntity user = userService.create(EntityUtil.prepareUser());
//        VacancyEntity vacancy = vacancyService.create(EntityUtil.prepareVacancy(user));
//
//        //when
//        VacancyEntity foundedVacancy = vacancyService.findById(vacancy.getId());
//
//        //then
//        Assertions.assertEquals(vacancy.getId(), foundedVacancy.getId());
//
//    }

//    @Test
//    public void findAll_happyPass() {
//        //give
//        UserEntity user = userService.create(EntityUtil.prepareUser());
//        vacancyService.create(EntityUtil.prepareVacancy(user));
//        vacancyService.create(EntityUtil.prepareVacancy(user));
//        vacancyService.create(EntityUtil.prepareVacancy(user));
//
//        //when
//        List<VacancyEntity> vacancies = vacancyService.findAll();
//
//        //then
//        Assertions.assertEquals(3, vacancies.size());
//    }

//    @Test
//    public void delete_happyPass() {
//        //give
//        UserEntity user = userService.create(EntityUtil.prepareUser());
//        VacancyEntity vacancyToDelete = vacancyService.create(EntityUtil.prepareVacancy(user));
//
//        //when
//        vacancyService.deleteById(vacancyToDelete.getId());
//
//        //then
//        VacancyEntity foundVacancy = vacancyRepository.findById(vacancyToDelete.getId());
//        Assertions.assertNull(foundVacancy);
//    }

//    @Test
//    public void deleteWithInterview_happyPass() {
//        //give
//        UserEntity user = userService.create(EntityUtil.prepareUser());
//        VacancyEntity vacancyToDelete = vacancyService.create(EntityUtil.prepareVacancy(user));
//        InterviewEntity interview = interviewService.create(EntityUtil.prepareInterview(user, vacancyToDelete));
//
//        //when
//        vacancyService.deleteById(vacancyToDelete.getId());
//
//        //then
//        VacancyEntity foundVacancy = vacancyRepository.findById(vacancyToDelete.getId());
//        Assertions.assertNull(foundVacancy);
//    }
}
