package by.itstep.job_search.service;

import by.itstep.job_search.dto.userDto.UserDto;
import by.itstep.job_search.dto.userDto.UserShortDto;
import by.itstep.job_search.enam.RoleOfUser;
import by.itstep.job_search.entity.InterviewEntity;
import by.itstep.job_search.entity.UserEntity;
import by.itstep.job_search.entity.VacancyEntity;
import by.itstep.job_search.repository.interfaceOfRepository.InterviewRepository;
import by.itstep.job_search.repository.interfaceOfRepository.UserRepository;
import by.itstep.job_search.util.DBCleaner;
import by.itstep.job_search.util.EntityUtil;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private InterviewService interviewService;

    @Autowired
    private VacancyService vacancyService;

    @Autowired
    private DBCleaner dbCleaner;

    @Autowired
    private UserRepository userRepository;


    @BeforeEach
    public void SetUp() {
        dbCleaner.clean();
    }

//    @AfterEach
//    public void ShutDown() {
//        dbCleaner.clean();
//    }

//    @Test
//    public void create_happyPass() {
//        //given
//        UserEntity user = EntityUtil.prepareUser();
//
//        //when
//        UserEntity createdUser = userService.create(user);
//
//        //then
//        Assertions.assertNotNull(createdUser.getId());
//        UserDto foundUser = userService.findById(createdUser.getId());
//        Assertions.assertEquals(user.getFirstName(), foundUser.getFirstName());
//        Assertions.assertEquals(user.getLastName(), foundUser.getLastName());
//        Assertions.assertEquals(user.getEmail(), foundUser.getEmail());
//        Assertions.assertEquals(user.getPhone(), foundUser.getPhone());
//        Assertions.assertEquals(user.getYearsOfExperience(), foundUser.getYearsOfExperience());
//        Assertions.assertEquals(user.getPosition(), foundUser.getPosition());
//    }

//    @Test
//    public void findById_happyPass() {
//        //given
//        UserEntity user = userService.create(EntityUtil.prepareUser());
//
//        //when
//        UserDto foundedUser = userService.findById(user.getId());
//
//        //then
//        Assertions.assertEquals(foundedUser.getId(), user.getId());
//    }

//    @Test
//    public void findAll_happyPass() {
//        //given
//        userService.create(EntityUtil.prepareUser());
//        userService.create(EntityUtil.prepareUser());
//        userService.create(EntityUtil.prepareUser());
//
//        //when
//        List<UserShortDto> users = userService.findAll();
//
//        //then
//        Assertions.assertEquals(3, users.size());
//    }

//    @Test
//    public void update_happyPass() {
//        //give
//        Faker faker = new Faker();
//        UserEntity existedUser = userService.create(EntityUtil.prepareUser());
//        existedUser.setRole(RoleOfUser.CANDIDATE);
//        existedUser.setFirstName(faker.name().firstName());
//        existedUser.setLastName(faker.name().lastName());
//        existedUser.setPhone(faker.phoneNumber().phoneNumber());
//        existedUser.setEmail(faker.internet().emailAddress());
//        existedUser.setPassword(faker.beer().name());
//        existedUser.setYearsOfExperience(faker.number().numberBetween(0, 20));
//        existedUser.setPosition(faker.company().profession());
//
//        //when
//        UserEntity updatedUser = userService.update(existedUser);
//
//        //then
//        Assertions.assertEquals(updatedUser.getId(), existedUser.getId());
//        UserDto foundEntity = userService.findById(updatedUser.getId());
//        Assertions.assertEquals(foundEntity.getFirstName(), existedUser.getFirstName());
//        Assertions.assertEquals(foundEntity.getLastName(), existedUser.getLastName());
//        Assertions.assertEquals(foundEntity.getEmail(), existedUser.getEmail());
//        Assertions.assertEquals(foundEntity.getPhone(), existedUser.getPhone());
//        Assertions.assertEquals(foundEntity.getYearsOfExperience(), existedUser.getYearsOfExperience());
//        Assertions.assertEquals(foundEntity.getPosition(), existedUser.getPosition());
//    }

//    @Test
//    public void delete_happyPass() {
//        //give
//        UserEntity userForDelete = userService.create(EntityUtil.prepareUser());
//
//        //when
//        userService.deleteById(userForDelete.getId());
//
//        //then
//        Assertions.assertNull(userRepository.findById(userForDelete.getId()));
//    }

//    @Test
//    public void deleteWithInterviewVacancy_happyPass() {
//        //give
//        UserEntity userForDelete = userService.create(EntityUtil.prepareUser());
//        VacancyEntity vacancy = vacancyService.create(EntityUtil.prepareVacancy(userForDelete));
//        InterviewEntity interview = interviewService.create(EntityUtil.prepareInterview(userForDelete, vacancy));
//
//        //when
//        userService.deleteById(userForDelete.getId());
//
//        //then
//        Assertions.assertNull(userRepository.findById(userForDelete.getId()));
//    }
}
